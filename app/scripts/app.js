'use strict';

angular.module('emporioApp', [
  // Angular modules
  // 'ngRoute',
  'ui.router',
  'ngAnimate',
  'dialogs',
  'ngynSelect2',

  // // 3rd Party Modules
  'ui.bootstrap',
  'easypiechart',
  'ui.tree',
  'ngMap',
  'ngTagsInput',
  'textAngular',
  'angular-loading-bar',
  'ui.calendar',
  'colorpicker.module',
  'angular-underscore',
  'restangular',

  // // Custom modules
  'emporioApp.nav',
  'emporioApp.localization',
  'emporioApp.chart',
  'emporioApp.ui',
  'emporioApp.ui.form',
  'emporioApp.ui.form.validation',
  'emporioApp.ui.map',
  'emporioApp.page',
  'emporioApp.business',
  'emporioApp.client',
  'emporioApp.publisher',
  'emporioApp.sellReport',
  'emporioApp.financialReport',
  'emporioApp.task',
  'emporioApp.calendar',
  'emporioApp.user',
  'emporioApp.header'
])
.config(function($stateProvider, $urlRouterProvider) {
	// var access = AUTH_ROLES.accessLevels;

	$stateProvider
  .state('user', {
	  // abstract: true,
    url: '/',
	  templateUrl: '/views/main.html'
	  // data: {
	  // access: access.public
	  // }
	})
  .state('anon', {
	  // abstract: true,
	  // template: '<ui-view/>'
    templateUrl: '/modules/auth/signin.html'
	  // data: {
	  // access: access.anon
	  // }
	});

  $urlRouterProvider.otherwise("/login");

})
.directive("imageResize", [
  "$parse", function($parse) {
    return {
      link: function(scope, elm, attrs) {
        var imagePercent;
        imagePercent = $parse(attrs.imagePercent)(scope);
        return elm.one("load", function() {
          var canvas, ctx, neededHeight, neededWidth;
          neededHeight = elm.height() * imagePercent / 100;
          neededWidth = elm.width() * imagePercent / 100;
          canvas = document.createElement("canvas");
          canvas.width = neededWidth;
          canvas.height = neededHeight;
          ctx = canvas.getContext("2d");
          ctx.drawImage(elm[0], 0, 0, neededWidth, neededHeight);
          return elm.attr('src', canvas.toDataURL("image/jpeg"));
        });
      }
    };
  }
])
.run(['$rootScope', '$state', 'EmporioRestangular', function ($rootScope, $state, EmporioRestangular) {

  $state.go('anon.login')

  $rootScope.updateSidebarAndHeader = function (){
    EmporioRestangular.all('loggedPublisher').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(publisher) {
      $('#businessListLi').show();
      $('#loggedUserName').html(publisher[0].name)
      if(publisher[0].isAdmin){
        $('#clientListLi').show();
        $('#publisherListLi').show();
        $('#sellReportListLi').show();
        $('#financialReportLi').show();
      }
    }, function(publisher, err){
    });
  }

  // setTimeout(function(){ 
  //   if($state.current.name != 'anon.login'){
  //     document.getElementById('header').style.display = "";
  //     document.getElementById('content').style.backgroundColor = "#eaeaea";
  //   }else{
  //     document.getElementById('header').style.display = "none";
  //     document.getElementById('content').style.backgroundColor = "#515a63";
  //   }
  // }, 500);

  $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {

    if(toState.name == "user" && !localStorage.getItem('currentToken')){
      setTimeout(function(){ 
        $state.go('anon.login')
      }, 1000);
    };
    if(toState.name == "anon.login"){
      if(localStorage.getItem('currentToken')){
        event.preventDefault();
      }else{
        setTimeout(function(){ 
          document.getElementById('header').style.display = "none";
          document.getElementById('content').style.backgroundColor = "#515a63";
        }, 1000);
      }     
    }else{
      setTimeout(function(){ 
        document.getElementById('header').style.display = "";
        document.getElementById('content').style.backgroundColor = "#eaeaea";
        $rootScope.updateSidebarAndHeader()
      }, 1000);
    }

  });

  $rootScope.$on('feedbackMessage', function(evt, message){
    $.gritter.add({
      // (string | mandatory) the heading of the notification
      title: message.status === 'success' ? 'Sucesso' : 'Erro',

      // (string | mandatory) the text inside the notification
      text: message.text
    });
  });
}]);

var module = angular.module('emporioModel', ['restangular']);

module.factory('EmporioRestangular', function(Restangular) {

  return Restangular.withConfig(function(RestangularConfigurer) {
    RestangularConfigurer.setBaseUrl('http://104.236.226.179:8080/api/');
  });

});
