'use strict';

/**
 * @ngdoc function
 * @name emporioApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the emporioApp
 */
angular.module('emporioApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
