'use strict';

angular.module('emporioApp.user').factory('config', function(){
	return {salt : '$2a$12$IgRCVlOp6tCCOe0h8TqDe.'}
});
