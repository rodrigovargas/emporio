'use strict';

angular.module('emporioApp.header', [])

angular.module('emporioApp.header').controller('headerCtrl', ['$scope', '$filter','EmporioRestangular', '$stateParams', '$rootScope', '$state', 'config', headerCtrl]);
	function headerCtrl($scope, $filter, EmporioRestangular, $stateParams, $rootScope, $state, config) {

		$scope.logout = function(){
			localStorage.removeItem('currentToken');
			$state.go('anon.login')
		};

		EmporioRestangular.all('loggedPublisher').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(publisher) {
      $scope.publisher = publisher;
    }, function(publishers, err){
	    if(publishers.status === 401) {
	      console.log('Não Autorizado')
	      localStorage.removeItem('currentToken');
	      $state.go('anon.login')
	    }
    });

	};