'use strict';

angular.module('emporioApp.publisher').controller('publisherFormCtrl', ['$scope', '$filter','EmporioRestangular', '$stateParams', '$rootScope', '$state', 'config', publisherFormCtrl]);
	function publisherFormCtrl($scope, $filter, EmporioRestangular, $stateParams, $rootScope, $state, config) {

		$scope.publisher = {};
		$scope.saveBtn = true;

		if($stateParams.id){
		  EmporioRestangular.one('publisher' , $stateParams.id).get({}, {authorization: localStorage.getItem('currentToken')}).then( function(publisher) {
		    $scope.publisher = publisher;
		  }, function(publisher, err){
	      if(publisher.status === 401) {
	        console.log('Não Autorizado')
	        localStorage.removeItem('currentToken');
	        $state.go('anon.login')
	      }
      });
		}else{
			$scope.baseModel = EmporioRestangular.all('publisher');
		};

		$scope.publisher.isAdmin = false;

    $scope.adminCheckbox = function(){
      if($scope.publisher.isAdmin == false){
        $scope.publisher.isAdmin = true;
      }else{
        $scope.publisher.isAdmin = false;
      }
    };

	  $scope.save = function(){
	  	if($stateParams.id && $scope.password){
	  		$scope.publisher.password = $scope.password;
	  	}
	  	if(!$stateParams.id){
	  		$scope.publisher.password = $scope.password;
	  	}
	  	$scope.saveBtn = false;
	  	if($scope.publisher.name == undefined || $scope.publisher.name == '' || $scope.publisher.email == undefined || $scope.publisher.email == ''){
				$scope.saveBtn = true;
				$rootScope.$broadcast('feedbackMessage', {text: 'Favor preencher os campos Nome, E-mail e Senha.', status: 'error'});
			}else if(!$stateParams.id && $scope.password == undefined || $scope.password == ''){
				$scope.saveBtn = true;
				$rootScope.$broadcast('feedbackMessage', {text: 'Favor preencher o campo Senha.', status: 'error'});
			}else{
				var bcrypt = new bCrypt();
				var salt = config.salt
				if($stateParams.id && $scope.password == '' || $scope.password == undefined){
					$scope.publisher.put({}, {authorization: localStorage.getItem('currentToken')}).then( function(publisher) {
				   	$scope.saveBtn = true;
				   	$rootScope.$broadcast('feedbackMessage', {text: 'Usuário salvo com sucesso!', status: 'success'});
				   	$state.go('user.publisherList')
				  }, function(publisher, err){
				  	$scope.saveBtn = true;
				  	$rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao salvar o usuário!', status: 'error'});
				  	if(business.status === 401) {
				      console.log('Não Autorizado')
				      localStorage.removeItem('currentToken');
				      $state.go('anon.login')
				    }
				  });
				}else{
					bcrypt.hashpw($scope.publisher.password, salt, function(hash){
						$scope.publisher.password = hash;
						if($stateParams.id){
			 				$scope.publisher.put({}, {authorization: localStorage.getItem('currentToken')}).then( function(publisher) {
						   	$scope.saveBtn = true;
						   	$rootScope.$broadcast('feedbackMessage', {text: 'Usuário salvo com sucesso!', status: 'success'});
						   	$state.go('user.publisherList')
						  }, function(publisher, err){
						  	$scope.saveBtn = true;
						  	$rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao salvar o usuário!', status: 'error'});
						  	if(business.status === 401) {
						      console.log('Não Autorizado')
						      localStorage.removeItem('currentToken');
						      $state.go('anon.login')
						    }
						  });
				  	}else{
				  		$scope.baseModel.post($scope.publisher, {}, {authorization: localStorage.getItem('currentToken')}).then( function(publisher) {
						   	$scope.saveBtn = true;
						   	$rootScope.$broadcast('feedbackMessage', {text: 'Usuário salvo com sucesso!', status: 'success'});
						   	$state.go('user.publisherList')
						  }, function(publisher, err){
						  	$scope.saveBtn = true;
						   	$rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao salvar o usuário!', status: 'error'});
						   	if(business.status === 401) {
						      console.log('Não Autorizado')
						      localStorage.removeItem('currentToken');
						      $state.go('anon.login')
						    }
						  });
				  	};
					}, function(hash, error) {
					 	// $rootScope.$broadcast('feedbackMessage', {text: 'Erro ao criptografar senha!', status: 'error'});
					});
				}						  	
		  };
	  };

	};