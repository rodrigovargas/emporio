'use strict';

angular.module('emporioApp.publisher').controller('publisherListCtrl', ['$scope', '$filter', 'EmporioRestangular', '$rootScope', '$dialogs', '$state', publisherListCtrl]);

  function publisherListCtrl($scope, $filter, EmporioRestangular, $rootScope, $dialogs, $state) {
    var init;

    $scope.init = function(){
      EmporioRestangular.all('publisher').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(publisher) {
      
        $scope.publisher = publisher;

        $scope.searchKeywords = '';

        $scope.filteredStores = [];

        $scope.row = '';

        $scope.remove = function(publisher){
          var dlg = $dialogs.confirm('Por favor confirme','Deseja realmente remover?');
          dlg.result.then(function(btn){
            EmporioRestangular.one('publisher' , publisher._id).remove({}, {authorization: localStorage.getItem('currentToken')}).then( function(publisher) {
              $rootScope.$broadcast('feedbackMessage', {text: 'Usuário removido com sucesso!', status: 'success'});
              $scope.init();
            }, function(publisher, err){
              $rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao remover o usuário!', status: 'error'});
              if(publisher.status === 401) {
                console.log('Não Autorizado')
                localStorage.removeItem('currentToken');
                $state.go('anon.login')
              }
            }); 
          });
        };

        $scope.select = function(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            return $scope.itens = $scope.filteredStores.slice(start, end);
        };

        $scope.onFilterChange = function() {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };

        $scope.onNumPerPageChange = function() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.onOrderChange = function() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.search = function() {
            $scope.filteredStores = $filter('filter')($scope.publisher, $scope.searchKeywords);
            return $scope.onFilterChange();
        };

        $scope.order = function(rowName) {
            if ($scope.row === rowName) {
                return;
            }
            $scope.row = rowName;
            $scope.filteredStores = $filter('orderBy')($scope.publisher, rowName);
            return $scope.onOrderChange();
        };

        $scope.numPerPageOpt = [3, 5, 10, 20];

        $scope.numPerPage = $scope.numPerPageOpt[2];

        $scope.currentPage = 1;

        $scope.itens = [];

        init = function() {
            $scope.search();
            return $scope.select($scope.currentPage);
        };

        init();
      }, function(publisher, err){
        if(publisher.status === 401) {
          console.log('Não Autorizado')
          localStorage.removeItem('currentToken');
          $state.go('anon.login')
        }
      });
    }

    $scope.init();

  }