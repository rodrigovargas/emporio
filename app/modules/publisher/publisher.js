'use strict';

angular.module('emporioApp.publisher', ['emporioModel'])
	.config(function($stateProvider) {

	  $stateProvider.state('user.publisherList', {
	    url: 'publisher',
	    templateUrl: '/modules/publisher/publisherList.html',
	    authenticate: true
	  });

	  $stateProvider.state('user.publisherForm', {
	    url: 'publisher/form',
	    templateUrl: '/modules/publisher/publisherForm.html',
	    authenticate: true
	  });

	  $stateProvider.state('user.publisherForm.edit', {
	    url: '/:id',
	    templateUrl: '/modules/publisher/publisherForm.html',
	    authenticate: true
	  });

	})
