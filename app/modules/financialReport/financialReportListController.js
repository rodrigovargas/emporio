'use strict';

angular.module('emporioApp.financialReport').controller('financialReportListCtrl', ['$scope', '$filter', 'EmporioRestangular', '$rootScope', '$dialogs', '$state', '$q', financialReportListCtrl]);

  function financialReportListCtrl($scope, $filter, EmporioRestangular, $rootScope, $dialogs, $state, $q) {
    var init;

    $scope.init = function(){
      EmporioRestangular.all('financialReport').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(financialReport) {
      
        $scope.financialReport = financialReport;

        for (var i = 0; i < $scope.financialReport.length; i++) {
            $scope.financialReport[i].date = moment($scope.financialReport[i].date, 'YYYY-MM-DD').format('DD/MM/YYYY')
        };

        $scope.searchKeywords = '';

        $scope.filteredStores = [];

        $scope.row = '';

        $scope.select = function(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            $scope.itens = $scope.filteredStores.slice(start, end);
            return $scope.itens
        };

        $scope.$watch('itens',
          function(newValue, oldValue) {
            $scope.balance = 0;
            for (var i = 0; i < $scope.itens.length; i++) {
                if($scope.itens[i].type == 'Compra'){
                    $scope.balance = $scope.balance - parseInt($scope.itens[i].value)
                }else{
                    $scope.balance = $scope.balance + parseInt($scope.itens[i].value)
                }
            };
        });

        $scope.onFilterChange = function() {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };

        $scope.onNumPerPageChange = function() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.onOrderChange = function() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.search = function() {
            $scope.filteredStores = $filter('filter')($scope.financialReport, $scope.searchKeywords);
            return $scope.onFilterChange();
        };

        $scope.order = function(rowName) {
            if ($scope.row === rowName) {
                return;
            }
            $scope.row = rowName;
            $scope.filteredStores = $filter('orderBy')($scope.financialReport, rowName);
            return $scope.onOrderChange();
        };

        $scope.numPerPageOpt = [3, 5, 10, 20];

        $scope.numPerPage = $scope.numPerPageOpt[2];

        $scope.currentPage = 1;

        $scope.itens = [];

        init = function() {
            $scope.search();
            return $scope.select($scope.currentPage);
        };

        init();
      }, function(financialReport, err){
        if(financialReport.status === 401) {
          console.log('Não Autorizado')
          localStorage.removeItem('currentToken');
          $state.go('anon.login')
        }
      });
    }

    $scope.init();

    //DATEPICKER
    $scope.open = function($event,opened) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope[opened] = true;
    };  
    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    //DATEPICKER

    $scope.$watch('startDate', function(oldValue, newValue) {
      if(oldValue && $scope.verifyDate() && $scope.startDate && $scope.endDate) {
        $scope.reloadFinancialReports().then(function(financialReport){
          $scope.itens = financialReport;
        });
      }
    });

    $scope.$watch('endDate', function(oldValue, newValue) {
      if(oldValue && $scope.verifyDate() && $scope.startDate && $scope.endDate) {
        $scope.reloadFinancialReports().then(function(financialReport){
          $scope.itens = financialReport;
        });
      }
    });

    $scope.verifyDate = function(){
      if(moment($scope.startDate).diff(moment($scope.endDate)) <= 0){
        return true
      }else{
        $rootScope.$broadcast('feedbackMessage', {text: 'A data de fim não pode ser menor que a de início!', status: 'error'});
      }
    };

  $scope.reloadFinancialReports = function() {
    var deferred = $q.defer();
    EmporioRestangular.all('loggedPublisher').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(publisher) {
      $scope.publisher = publisher;
      EmporioRestangular.all('financialReport').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(financialReport) {
        $scope.financialReport = financialReport;
        for (var i = 0; i < $scope.financialReport.length; i++) {
            $scope.financialReport[i].date = moment($scope.financialReport[i].date, 'YYYY-MM-DD').format('DD/MM/YYYY')
        };
          var filteredPosts = _.reduce($scope.financialReport, function(filtered, element){
            if (moment(element.date, 'YYYY-MM-DD').diff($scope.startDate, 'seconds', true) >= 0 && moment(element.date, 'YYYY-MM-DD').diff($scope.endDate, 'seconds', true) <= 0) {
              filtered.push(element)
            }; 
            return filtered;
          }, []);
          console.log('filteredPosts', filteredPosts)
          filteredPosts  = _.sortBy(filteredPosts, function(element){
            return element.date;
          });
          filteredPosts = filteredPosts.reverse();
          deferred.resolve(filteredPosts);
      }, function(financialReport, err){
        if(financialReport.status === 401) {
          console.log('Não Autorizado')
          localStorage.removeItem('currentToken');
          $state.go('anon.login')
        }
      });
    }, function(publisher, err){
      if(publisher.status === 401) {
        console.log('Não Autorizado')
        localStorage.removeItem('currentToken');
        $state.go('anon.login')
      }
    });

    return deferred.promise;
  };

  }