'use strict';

angular.module('emporioApp.financialReport', ['emporioModel'])
	.config(function($stateProvider) {

	  $stateProvider.state('user.financialReport', {
	    url: 'financialReport',
	    templateUrl: '/modules/financialReport/financialReportList.html',
	    authenticate: true
	  });

	})
