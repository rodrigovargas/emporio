'use strict';

angular.module('emporioApp.business', ['emporioModel', 'imageupload'])
	.config(function($stateProvider) {

	  $stateProvider.state('user.businessList', {
	    url: 'business',
	    templateUrl: '/modules/business/businessList.html',
	    authenticate: true
	  });

	  $stateProvider.state('user.businessForm', {
	    url: 'business/form',
	    templateUrl: '/modules/business/businessForm.html',
	    authenticate: true
	  });

	  $stateProvider.state('user.businessForm.edit', {
	    url: '/:id',
	    templateUrl: '/modules/business/businessForm.html',
	    authenticate: true
	  });

});
