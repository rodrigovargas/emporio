'use strict';

angular.module('emporioApp.business').controller('businessFormCtrl', ['$scope', '$filter','EmporioRestangular', '$stateParams', '$rootScope', '$timeout', '$q', '$state', '$http', '$dialogs', businessFormCtrl]);
	function businessFormCtrl($scope, $filter, EmporioRestangular, $stateParams, $rootScope, $timeout, $q, $state, $http, $dialogs) {

		$scope.business = {};
		$scope.saveBtn = true;

		$scope.financialReportBaseModel = EmporioRestangular.all('financialReport');
		$scope.financialReport = {};

		if($stateParams.id){
		  EmporioRestangular.one('business' , $stateParams.id).get({}, {authorization: localStorage.getItem('currentToken')}).then( function(business) {
		    $scope.business = business;
		    if(!$scope.business.services){
          $scope.business.services = [{}];
        }
		  }, function(business, err){
        if(business.status === 401) {
          console.log('Não Autorizado')
          localStorage.removeItem('currentToken');
          $state.go('anon.login')
        }
      });
		}else{
			$scope.baseModel = EmporioRestangular.all('business');
			$scope.business.services = [{}];
		};

		EmporioRestangular.all('client').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(client) {
      $scope.clients = client
    }, function(client, err){
      if(client.status === 401) {
        console.log('Não Autorizado')
        localStorage.removeItem('currentToken');
        $state.go('anon.login')
      }
    });

		setTimeout(function(){ 
			$("[name='date']").keypress(false);
			// $("[name='money']").maskMoney({symbol:'R$ ', showSymbol:true, thousands:'.', decimal:',', symbolStay: true, precision: 0});
		}, 500);

		$('#newClientDiv + div .btn-primary').click(function(){
			$scope.editClientModel = undefined;
			$scope.newCampaignName = '';
			$scope.newCampaignEmail = '';
			$scope.newCampaignPhone = '';
			$scope.newCampaignAddress = ''; 
			$scope.$apply();
		});

		// Service dynamic field
	  $scope.addServiceField = function(){
	    $scope.business.services.push({});
	  //   setTimeout(function(){ 
			// 	$("[name='money']").maskMoney({symbol:'R$ ', showSymbol:true, thousands:'.', decimal:',', symbolStay: true, precision: 0});
			// }, 500);
	  };
	  $scope.removeServiceField = function(service){
	    $scope.business.services = _.without($scope.business.services, service); 
	  };

		$scope.$watch('business.buyPrice',
      function(newValue, oldValue) {
      	$scope.calculateTotal();
      }
    );

    $scope.$watch('business.services',
      function(newValue, oldValue) {
      	$scope.calculateTotal();
      },true
    );

    $scope.calculateTotal = function(){
    	$scope.servicePrices = [];
    	var total = 0;
    	if($scope.business.services){
    		for (var i = 0; i < $scope.business.services.length; i++) {
    			if($scope.business.services[i].price){
    				$scope.servicePrices.push(parseInt($scope.business.services[i].price))
    			}
	    	};
    		$scope.servicePrices.push(parseInt($scope.business.buyPrice))
	    	total = $scope.servicePrices.reduce(function(prev, cur) {
          return prev + cur;
        });
    	}
      $scope.business.totalCost = total;
    };

    $scope.removeClient = function(client){
			if(client == undefined){
				$rootScope.$broadcast('feedbackMessage', {text: 'Selecione alguma campanha para remover!', status: 'error'});
			}else{
				var dlg = $dialogs.confirm('Por favor confirme','Deseja realmente remover?');
	      dlg.result.then(function(btn){
	        EmporioRestangular.one('client' , client).remove({}, {authorization: localStorage.getItem('currentToken')}).then( function(client) {
	          $rootScope.$broadcast('feedbackMessage', {text: 'Campanha removida com sucesso!', status: 'success'});
	          EmporioRestangular.all('client').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(clients) {
				      $scope.clients = clients;
				      $scope.business.clientToSell = '';
				    });
	        }, function(post, err){
	          $rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao remover a campanha!', status: 'error'});
	          if(post.status === 401) {
		          console.log('Não Autorizado')
		          localStorage.removeItem('currentToken');
		          $state.go('anon.login')
		        }
	        }); 
	      });
			};
    };

    $scope.editClient = function(client) {
    	if(client){
    		$('#myModal2').modal('toggle');
	    	EmporioRestangular.one('client' , client).get({}, {authorization: localStorage.getItem('currentToken')}).then( function(client) {
			    $scope.editClientModel = client;
			    $scope.newCampaignName = client.name;
			   	$scope.newCampaignEmail = client.email;
			   	$scope.newCampaignPhone = client.phone;
			   	$scope.newCampaignAddress = client.address;
			  }, function(client, err){
	        if(client.status === 401) {
	          console.log('Não Autorizado')
	          localStorage.removeItem('currentToken');
	          $state.go('anon.login')
	        }
	      });
    	}else{
    		$rootScope.$broadcast('feedbackMessage', {text: 'Selecione um cliente para editar.', status: 'error'});
    	}
    };

    $scope.modalSave = function(){
	  	$scope.modalSaveBtn = false;
	  	if($scope.newCampaignName == undefined || $scope.newCampaignName == '' || $scope.newCampaignEmail == undefined || $scope.newCampaignEmail == '' || $scope.newCampaignPhone == undefined || $scope.newCampaignPhone == '' || $scope.newCampaignAddress == undefined || $scope.newCampaignAddress == ''){ 
				$scope.modalSaveBtn = true;
				$rootScope.$broadcast('feedbackMessage', {text: 'Favor preencher todos os campos', status: 'error'});
			}else{
				$scope.campaignBaseModel = EmporioRestangular.all('client');
				$scope.campaign = {};
				$scope.campaign.name = $scope.newCampaignName;
				$scope.campaign.email = $scope.newCampaignEmail;
				$scope.campaign.phone = $scope.newCampaignPhone;
				$scope.campaign.address = $scope.newCampaignAddress;

				if($scope.editClientModel){
					$scope.editClientModel.name = $scope.newCampaignName;
					$scope.editClientModel.email = $scope.newCampaignEmail;
					$scope.editClientModel.phone = $scope.newCampaignPhone;
					$scope.editClientModel.address = $scope.newCampaignAddress;
					$scope.editClientModel.put({}, {authorization: localStorage.getItem('currentToken')}).then( function(business) {
			   		$rootScope.$broadcast('feedbackMessage', {text: 'Veículo salvo com sucesso!', status: 'success'});
			   		$scope.editClientModel = {};		
			   		$scope.campaign.name = '';
				   	$scope.newCampaignName = '';
				   	$scope.newCampaignEmail = '';
				   	$scope.newCampaignPhone = '';
				   	$scope.newCampaignAddress = '';
				   	EmporioRestangular.all('client').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(campaign) {
				      $scope.clients = campaign;
				      $('#myModal2').modal('toggle');
				    }, function(campaign, err){
					   	$rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao salvar o post!', status: 'error'});
					   	if(campaign.status === 401) {
				        console.log('Não Autorizado')
				        localStorage.removeItem('currentToken');
				        $state.go('anon.login')
				      }
					  });
				  }, function(business, err){
				  	$rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao salvar o veículo!', status: 'error'});
				  	if(business.status === 401) {
		          console.log('Não Autorizado')
		          localStorage.removeItem('currentToken');
		          $state.go('anon.login')
		        }
				  });
				}else{
					$scope.campaignBaseModel.post($scope.campaign, {}, {authorization: localStorage.getItem('currentToken')}).then( function(post) {
				   	$rootScope.$broadcast('feedbackMessage', {text: 'Post salvo com sucesso!', status: 'success'});
				   	$scope.campaign.name = '';
				   	$scope.newCampaignName = '';
				   	$scope.newCampaignEmail = '';
				   	$scope.newCampaignPhone = '';
				   	$scope.newCampaignAddress = '';
				   	EmporioRestangular.all('client').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(campaign) {
				      $scope.clients = campaign;
				      $('#myModal2').modal('toggle');
				    }, function(campaign, err){
					   	$rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao salvar o post!', status: 'error'});
					   	if(campaign.status === 401) {
				        console.log('Não Autorizado')
				        localStorage.removeItem('currentToken');
				        $state.go('anon.login')
				      }
					  });
				  }, function(post, err){
				  	$scope.saveBtn = true;
				   	$rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao salvar o post!', status: 'error'});
				   	if(post.status === 401) {
			        console.log('Não Autorizado')
			        localStorage.removeItem('currentToken');
			        $state.go('anon.login')
			      }
				  });
				};
		  };
	  };

	  $scope.save = function(sell){
	  	if (!$scope.business.name || !$scope.business.brand || !$scope.business.plate || !$scope.business.buyDate ) {
	  		$rootScope.$broadcast('feedbackMessage', {text: 'Favor preencher todos os campos obrigatórios.', status: 'error'});
	  	}else{
	  		if(sell == 'sell' && $scope.business.sellPrice && $scope.business.sellDate){
		  		$scope.business.sold = true;
		  		$scope.financialReport.name = $scope.business.name;
		  		$scope.financialReport.date = $scope.business.sellDate;
		  		$scope.financialReport.type = 'Venda';
		  		$scope.financialReport.value = $scope.business.sellPrice;
		  		$scope.financialReportBaseModel.post($scope.financialReport, {}, {authorization: localStorage.getItem('currentToken')}).then( function(financialReport) {
		  			console.log('Relatório Financeiro Criado')
				  }, function(financialReport, err){
				  	console.log('Erro ao criar Relatório Financeiro')
				   	if(financialReport.status === 401) {
		          console.log('Não Autorizado')
		          localStorage.removeItem('currentToken');
		          $state.go('anon.login')
		        }
				  });
				  $scope.doSave();
		  	};
		  	if(sell == 'sell' && !$scope.business.sellPrice) {
		  		$rootScope.$broadcast('feedbackMessage', {text: 'Favor preencher o campo "Valor da Venda".', status: 'error'});
		  	};
		  	if(sell == 'sell' && !$scope.business.sellDate){
		  		$rootScope.$broadcast('feedbackMessage', {text: 'Favor preencher o campo "Data da Venda".', status: 'error'});
		  	};
		  	if(!sell){
		  		$scope.doSave();
		  	}
	  	};
	  };

	  $scope.doSave = function(){
	  	$scope.saveBtn = false;
	  	if($stateParams.id){
 				$scope.business.put({}, {authorization: localStorage.getItem('currentToken')}).then( function(business) {
 					$scope.saveBtn = true;
		   		$rootScope.$broadcast('feedbackMessage', {text: 'Veículo salvo com sucesso!', status: 'success'});
		   		$state.go('user.businessList')	
		   		if($scope.exchange == true){
            $state.go('user.businessForm');
            $rootScope.$broadcast('feedbackMessage', {text: 'Cadastre agora o veículo da troca', status: 'error'});
          }		
			  }, function(business, err){
			  	$scope.saveBtn = true;
			  	$rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao salvar o veículo!', status: 'error'});
			  	if(business.status === 401) {
	          console.log('Não Autorizado')
	          localStorage.removeItem('currentToken');
	          $state.go('anon.login')
	        }
			  });
	  	}else{
	  		$scope.financialReport.name = $scope.business.name;
	  		$scope.financialReport.date = $scope.business.buyDate;
	  		$scope.financialReport.type = 'Compra';
	  		$scope.financialReport.value = $scope.business.buyPrice;
	  		$scope.financialReportBaseModel.post($scope.financialReport, {}, {authorization: localStorage.getItem('currentToken')}).then( function(financialReport) {
	  			console.log('Relatório Financeiro Criado')
			  }, function(financialReport, err){
			  	console.log('Erro ao criar Relatório Financeiro')
			   	if(financialReport.status === 401) {
	          console.log('Não Autorizado')
	          localStorage.removeItem('currentToken');
	          $state.go('anon.login')
	        }
			  });
	  		$scope.baseModel.post($scope.business, {}, {authorization: localStorage.getItem('currentToken')}).then( function(business) {
  				$scope.saveBtn = true;
		   		$rootScope.$broadcast('feedbackMessage', {text: 'Veículo salvo com sucesso!', status: 'success'});
		   		$state.go('user.businessList')
			  }, function(business, err){
			  	$scope.saveBtn = true;
			   	$rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao salvar o veículo!', status: 'error'});
			   	if(business.status === 401) {
	          console.log('Não Autorizado')
	          localStorage.removeItem('currentToken');
	          $state.go('anon.login')
	        }
			  });
			};
	  }

    $scope.exchange = false;

    $scope.scheduleCheckbox = function(){
      if($scope.exchange == false){
        $scope.exchange = true;
      }else{
        $scope.exchange = false;
      }
    };

	};	