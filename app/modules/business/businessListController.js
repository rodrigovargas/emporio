'use strict';

angular.module('emporioApp.business').controller('businessListCtrl', ['$scope', '$filter', 'EmporioRestangular', '$rootScope', '$dialogs', '$state', businessListCtrl]);

  function businessListCtrl($scope, $filter, EmporioRestangular, $rootScope, $dialogs, $state) {
    var init;

    $('#newClientDiv + div .btn-primary').click(function(){
      $scope.editClientModel = undefined;
      $scope.newCampaignName = '';
      $scope.newCampaignEmail = '';
      $scope.newCampaignPhone = '';
      $scope.newCampaignAddress = ''; 
      $scope.$apply();
    });

    $scope.init = function(){
      EmporioRestangular.all('business').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(business) {
      
        $scope.business = business;

        for (var i = 0; i < $scope.business.length; i++) {
          $scope.business[i].timeInStock = moment().diff(moment($scope.business[i].buyDate, 'YYYY-MM-DD'), 'days')
        };

        $scope.business = _.reduce($scope.business, function(filteredBusiness, element){
          if(!element.sold){
            filteredBusiness.push(element);
          }
          return filteredBusiness;
        }, []);

        $scope.searchKeywords = '';

        $scope.filteredStores = [];

        $scope.row = '';

        EmporioRestangular.all('client').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(client) {
          $scope.clients = client
        }, function(client, err){
          if(client.status === 401) {
            console.log('Não Autorizado')
            localStorage.removeItem('currentToken');
            $state.go('anon.login')
          }
        });

        setTimeout(function(){ 
          $("[name='date']").keypress(false);
          // $("[name='money']").maskMoney({symbol:'R$ ', showSymbol:true, thousands:'.', decimal:',', symbolStay: true, precision: 0});
        }, 500);

        $scope.editClient = function(client) {
          if(client){
            $('#myModal2').modal('toggle');
            EmporioRestangular.one('client' , client).get({}, {authorization: localStorage.getItem('currentToken')}).then( function(client) {
              $scope.editClientModel = client;
              $scope.newCampaignName = client.name;
              $scope.newCampaignEmail = client.email;
              $scope.newCampaignPhone = client.phone;
              $scope.newCampaignAddress = client.address;
            }, function(client, err){
              if(client.status === 401) {
                console.log('Não Autorizado')
                localStorage.removeItem('currentToken');
                $state.go('anon.login')
              }
            });
          }else{
            $rootScope.$broadcast('feedbackMessage', {text: 'Selecione um cliente para editar.', status: 'error'});
          }
        };

        $scope.modalSave = function(){
          $scope.modalSaveBtn = false;
          if($scope.newCampaignName == undefined || $scope.newCampaignName == '' || $scope.newCampaignEmail == undefined || $scope.newCampaignEmail == '' || $scope.newCampaignPhone == undefined || $scope.newCampaignPhone == '' || $scope.newCampaignAddress == undefined || $scope.newCampaignAddress == ''){ 
            $scope.modalSaveBtn = true;
            $rootScope.$broadcast('feedbackMessage', {text: 'Favor preencher todos os campos', status: 'error'});
          }else{
            $scope.campaignBaseModel = EmporioRestangular.all('client');
            $scope.campaign = {};
            $scope.campaign.name = $scope.newCampaignName;
            $scope.campaign.email = $scope.newCampaignEmail;
            $scope.campaign.phone = $scope.newCampaignPhone;
            $scope.campaign.address = $scope.newCampaignAddress;

            if($scope.editClientModel){
              $scope.editClientModel.name = $scope.newCampaignName;
              $scope.editClientModel.email = $scope.newCampaignEmail;
              $scope.editClientModel.phone = $scope.newCampaignPhone;
              $scope.editClientModel.address = $scope.newCampaignAddress;
              $scope.editClientModel.put({}, {authorization: localStorage.getItem('currentToken')}).then( function(business) {
                $rootScope.$broadcast('feedbackMessage', {text: 'Veículo salvo com sucesso!', status: 'success'});
                $scope.editClientModel = {};    
                $scope.campaign.name = '';
                $scope.newCampaignName = '';
                $scope.newCampaignEmail = '';
                $scope.newCampaignPhone = '';
                $scope.newCampaignAddress = '';
                EmporioRestangular.all('client').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(campaign) {
                  $scope.clients = campaign;
                  $('#myModal2').modal('toggle');
                }, function(campaign, err){
                  $rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao salvar o post!', status: 'error'});
                  if(campaign.status === 401) {
                    console.log('Não Autorizado')
                    localStorage.removeItem('currentToken');
                    $state.go('anon.login')
                  }
                });
              }, function(business, err){
                $rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao salvar o veículo!', status: 'error'});
                if(business.status === 401) {
                  console.log('Não Autorizado')
                  localStorage.removeItem('currentToken');
                  $state.go('anon.login')
                }
              });
            }else{
              $scope.campaignBaseModel.post($scope.campaign, {}, {authorization: localStorage.getItem('currentToken')}).then( function(post) {
                $rootScope.$broadcast('feedbackMessage', {text: 'Post salvo com sucesso!', status: 'success'});
                $scope.campaign.name = '';
                $scope.newCampaignName = '';
                $scope.newCampaignEmail = '';
                $scope.newCampaignPhone = '';
                $scope.newCampaignAddress = '';
                EmporioRestangular.all('client').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(campaign) {
                  $scope.clients = campaign;
                  $('#myModal2').modal('toggle');
                }, function(campaign, err){
                  $rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao salvar o post!', status: 'error'});
                  if(campaign.status === 401) {
                    console.log('Não Autorizado')
                    localStorage.removeItem('currentToken');
                    $state.go('anon.login')
                  }
                });
              }, function(post, err){
                $scope.saveBtn = true;
                $rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao salvar o post!', status: 'error'});
                if(post.status === 401) {
                  console.log('Não Autorizado')
                  localStorage.removeItem('currentToken');
                  $state.go('anon.login')
                }
              });
            };
          };
        };

        $scope.remove = function(business){
          var dlg = $dialogs.confirm('Por favor confirme','Deseja realmente remover?');
          dlg.result.then(function(btn){
            EmporioRestangular.one('business' , business._id).remove({}, {authorization: localStorage.getItem('currentToken')}).then( function(business) {
              $rootScope.$broadcast('feedbackMessage', {text: 'Veículo removido com sucesso!', status: 'success'});
              $scope.init();
            }, function(business, err){
              $rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao remover o veículo!', status: 'error'});
              if(business.status === 401) {
                console.log('Não Autorizado')
                localStorage.removeItem('currentToken');
                $state.go('anon.login')
              }
            }); 
          });
        };

        $scope.sell = function (){
          if(!$scope.businessToSave.sellPrice){
            $rootScope.$broadcast('feedbackMessage', {text: 'Favor preencher o campo "Valor da Venda".', status: 'error'});
          }else if(!$scope.businessToSave.sellDate){
            $rootScope.$broadcast('feedbackMessage', {text: 'Favor preencher o campo "Data da Venda".', status: 'error'});
          }else{
            $scope.businessToSave.sold = true;
            $scope.businessToSave.put({}, {authorization: localStorage.getItem('currentToken')}).then( function(business) {
              $rootScope.$broadcast('feedbackMessage', {text: 'Veículo salvo com sucesso!', status: 'success'});
              $scope.init();
              $('#myModal').modal('hide')
              $scope.businessToSave = {};
              if($scope.exchange == true){
                $state.go('user.businessForm');
                $rootScope.$broadcast('feedbackMessage', {text: 'Cadastre agora o veículo da troca', status: 'error'});
              }
            }, function(business, err){
              $rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao salvar o veículo!', status: 'error'});
              if(business.status === 401) {
                console.log('Não Autorizado')
                localStorage.removeItem('currentToken');
                $state.go('anon.login')
              }
            });
          }
        };

        $scope.exchange = false;

        $scope.scheduleCheckbox = function(){
          if($scope.exchange == false){
            $scope.exchange = true;
          }else{
            $scope.exchange = false;
          }
        };

        $scope.itemToSell = function(item){
          EmporioRestangular.one('business' , item._id).get({}, {authorization: localStorage.getItem('currentToken')}).then( function(business) {
            $scope.businessToSave = business;
          }, function(business, err){
            if(business.status === 401) {
              console.log('Não Autorizado')
              localStorage.removeItem('currentToken');
              $state.go('anon.login')
            }
          });
        };

        $scope.select = function(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            return $scope.itens = $scope.filteredStores.slice(start, end);
        };

        $scope.onFilterChange = function() {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };

        $scope.onNumPerPageChange = function() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.onOrderChange = function() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.search = function() {
            $scope.filteredStores = $filter('filter')($scope.business, $scope.searchKeywords);
            return $scope.onFilterChange();
        };

        $scope.order = function(rowName) {
            if ($scope.row === rowName) {
                return;
            }
            $scope.row = rowName;
            $scope.filteredStores = $filter('orderBy')($scope.business, rowName);
            return $scope.onOrderChange();
        };

        $scope.numPerPageOpt = [3, 5, 10, 20];

        $scope.numPerPage = $scope.numPerPageOpt[2];

        $scope.currentPage = 1;

        $scope.itens = [];

        init = function() {
            $scope.search();
            return $scope.select($scope.currentPage);
        };

        init();
      }, function(business, err){
        if(business.status === 401) {
          console.log('Não Autorizado')
          localStorage.removeItem('currentToken');
          $state.go('anon.login')
        }
      });
    }

    $scope.init();

  }