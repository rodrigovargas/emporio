'use strict';

angular.module('emporioApp.client').controller('clientListCtrl', ['$scope', '$filter', 'EmporioRestangular', '$rootScope', '$dialogs', '$state', clientListCtrl]);

  function clientListCtrl($scope, $filter, EmporioRestangular, $rootScope, $dialogs, $state) {
    var init;

    $scope.init = function(){
      EmporioRestangular.all('client').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(client) {
      
        $scope.client = client;

        for (var i = 0; i < $scope.client.length; i++) {
          $scope.client[i].timeInStock = moment().diff(moment($scope.client[i].buyDate, 'YYYY-MM-DD'), 'days')
        };

        $scope.client = _.reduce($scope.client, function(filteredClient, element){
          if(!element.sold){
            filteredClient.push(element);
          }
          return filteredClient;
        }, []);

        $scope.searchKeywords = '';

        $scope.filteredStores = [];

        $scope.row = '';

        EmporioRestangular.all('client').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(client) {
          $scope.clients = client
        }, function(client, err){
          if(client.status === 401) {
            console.log('Não Autorizado')
            localStorage.removeItem('currentToken');
            $state.go('anon.login')
          }
        });

        setTimeout(function(){ 
          $("[name='date']").keypress(false);
          // $("[name='money']").maskMoney({symbol:'R$ ', showSymbol:true, thousands:'.', decimal:',', symbolStay: true, precision: 0});
        }, 500);

        $scope.remove = function(client){
          var dlg = $dialogs.confirm('Por favor confirme','Deseja realmente remover?');
          dlg.result.then(function(btn){
            EmporioRestangular.one('client' , client._id).remove({}, {authorization: localStorage.getItem('currentToken')}).then( function(client) {
              $rootScope.$broadcast('feedbackMessage', {text: 'Veículo removido com sucesso!', status: 'success'});
              $scope.init();
            }, function(client, err){
              $rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao remover o veículo!', status: 'error'});
              if(client.status === 401) {
                console.log('Não Autorizado')
                localStorage.removeItem('currentToken');
                $state.go('anon.login')
              }
            }); 
          });
        };

        $scope.select = function(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            return $scope.itens = $scope.filteredStores.slice(start, end);
        };

        $scope.onFilterChange = function() {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };

        $scope.onNumPerPageChange = function() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.onOrderChange = function() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.search = function() {
            $scope.filteredStores = $filter('filter')($scope.client, $scope.searchKeywords);
            return $scope.onFilterChange();
        };

        $scope.order = function(rowName) {
            if ($scope.row === rowName) {
                return;
            }
            $scope.row = rowName;
            $scope.filteredStores = $filter('orderBy')($scope.client, rowName);
            return $scope.onOrderChange();
        };

        $scope.numPerPageOpt = [3, 5, 10, 20];

        $scope.numPerPage = $scope.numPerPageOpt[2];

        $scope.currentPage = 1;

        $scope.itens = [];

        init = function() {
            $scope.search();
            return $scope.select($scope.currentPage);
        };

        init();
      }, function(client, err){
        if(client.status === 401) {
          console.log('Não Autorizado')
          localStorage.removeItem('currentToken');
          $state.go('anon.login')
        }
      });
    }

    $scope.init();

  }