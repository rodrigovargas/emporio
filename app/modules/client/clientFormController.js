'use strict';

angular.module('emporioApp.client').controller('clientFormCtrl', ['$scope', '$filter','EmporioRestangular', '$stateParams', '$rootScope', '$timeout', '$q', '$state', '$http', '$dialogs', clientFormCtrl]);
	function clientFormCtrl($scope, $filter, EmporioRestangular, $stateParams, $rootScope, $timeout, $q, $state, $http, $dialogs) {

		$scope.client = {};
		$scope.saveBtn = true;

		if($stateParams.id){
		  EmporioRestangular.one('client' , $stateParams.id).get({}, {authorization: localStorage.getItem('currentToken')}).then( function(client) {
		    $scope.client = client;
		  }, function(client, err){
        if(client.status === 401) {
          console.log('Não Autorizado')
          localStorage.removeItem('currentToken');
          $state.go('anon.login')
        }
      });
		}else{
			$scope.baseModel = EmporioRestangular.all('client');
		};

	  $scope.save = function(sell){
	  	if (!$scope.client.name || !$scope.client.email || !$scope.client.phone || !$scope.client.address || !$scope.client.cpf || !$scope.client.rg) {
	  		$rootScope.$broadcast('feedbackMessage', {text: 'Favor preencher todos os campos obrigatórios.', status: 'error'});
	  	}else{
		  	$scope.doSave();
	  	};
	  };

	  $scope.doSave = function(){
	  	$scope.saveBtn = false;
	  	if($stateParams.id){
 				$scope.client.put({}, {authorization: localStorage.getItem('currentToken')}).then( function(client) {
 					$scope.saveBtn = true;
		   		$rootScope.$broadcast('feedbackMessage', {text: 'Cliente salvo com sucesso!', status: 'success'});
		   		$state.go('user.clientList')	
		   		if($scope.exchange == true){
            $state.go('user.clientForm');
            $rootScope.$broadcast('feedbackMessage', {text: 'Cadastre agora o cliente da troca', status: 'error'});
          }		
			  }, function(client, err){
			  	$scope.saveBtn = true;
			  	$rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao salvar o cliente!', status: 'error'});
			  	if(client.status === 401) {
	          console.log('Não Autorizado')
	          localStorage.removeItem('currentToken');
	          $state.go('anon.login')
	        }
			  });
	  	}else{
	  		$scope.baseModel.post($scope.client, {}, {authorization: localStorage.getItem('currentToken')}).then( function(client) {
  				$scope.saveBtn = true;
		   		$rootScope.$broadcast('feedbackMessage', {text: 'Cliente salvo com sucesso!', status: 'success'});
		   		$state.go('user.clientList')
			  }, function(client, err){
			  	$scope.saveBtn = true;
			   	$rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao salvar o cliente!', status: 'error'});
			   	if(client.status === 401) {
	          console.log('Não Autorizado')
	          localStorage.removeItem('currentToken');
	          $state.go('anon.login')
	        }
			  });
			};
	  }
	};	