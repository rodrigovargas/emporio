'use strict';

angular.module('emporioApp.client', ['emporioModel', 'imageupload'])
	.config(function($stateProvider) {

	  $stateProvider.state('user.clientList', {
	    url: 'client',
	    templateUrl: '/modules/client/clientList.html',
	    authenticate: true
	  });

	  $stateProvider.state('user.clientForm', {
	    url: 'client/form',
	    templateUrl: '/modules/client/clientForm.html',
	    authenticate: true
	  });

	  $stateProvider.state('user.clientForm.edit', {
	    url: '/:id',
	    templateUrl: '/modules/client/clientForm.html',
	    authenticate: true
	  });

});
