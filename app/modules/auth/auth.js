'use strict';

var userModule = angular.module('emporioApp.user', [])
.config(function($stateProvider) {
	
  $stateProvider.state('anon.login', {
    url: '/login',
    templateUrl: '/modules/auth/signin.html'
  });

});