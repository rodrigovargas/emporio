'use strict';

angular.module('emporioApp.user').controller('authCtrl', ['$scope', '$state', '$q', '$http', '$rootScope', 'config', authCtrl]);
	function authCtrl($scope, $state, $q, $http, $rootScope, config) {

		$scope.login = function(){
			$scope.spin = true;

			var bcrypt = new bCrypt();
			var salt = config.salt

			bcrypt.hashpw($scope.password, salt, function(hash){
				$http.post('http://104.236.226.179:8080/api/login', {username: $scope.username, password: hash}).
			  success(function(data, status, headers, config) {
			    document.getElementById('header').style.display = "";
					setTimeout(function(){ document.getElementById('content').style.backgroundColor = "#eaeaea";}, 100);
		  		$state.go('user');
		  		localStorage.setItem("currentToken", data.response.token);
		  		console.log('Login Ok, currentToken:', localStorage.getItem("currentToken"))
		  		$scope.spin = false;
			  }).
			  error(function(data, status, headers, config) {
			    $rootScope.$broadcast('feedbackMessage', {text: 'Usuário e/ou senha invalidos', status: 'error'});
			    $scope.spin = false;
			  });
			}, function() {});

		};

	};