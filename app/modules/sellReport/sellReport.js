'use strict';

angular.module('emporioApp.sellReport', ['emporioModel'])
	.config(function($stateProvider) {

	  $stateProvider.state('user.sellReportList', {
	    url: 'sellReport',
	    templateUrl: '/modules/sellReport/sellReportList.html',
	    authenticate: true
	  });

	})
