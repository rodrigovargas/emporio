'use strict';

angular.module('emporioApp.sellReport').controller('sellReportListCtrl', ['$scope', '$filter', 'EmporioRestangular', '$rootScope', '$dialogs', '$state', '$q', sellReportListCtrl]);

  function sellReportListCtrl($scope, $filter, EmporioRestangular, $rootScope, $dialogs, $state, $q) {
    var init;

    $scope.init = function(){
      EmporioRestangular.all('business').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(sellReport) {
      
        $scope.sellReport = sellReport;

        $scope.sellReport = _.reduce($scope.sellReport, function(filtered, element){
          if (element.sellDate) {
            filtered.push(element)
          }; 
          return filtered;
        }, []);

        for (var i = 0; i < $scope.sellReport.length; i++) {
            $scope.sellReport[i].servicesCost = [];
            $scope.sellReport[i].timeInStock = moment($scope.sellReport[i].sellDate, 'YYYY-MM-DD').diff(moment($scope.sellReport[i].buyDate, 'YYYY-MM-DD'), 'days')
            for (var j = 0; j < $scope.sellReport[i].services.length; j++) {
                $scope.sellReport[i].servicesCost.push(parseInt($scope.sellReport[i].services[j].price))
            }; 

            $scope.sellReport[i].servicesCost = $scope.sellReport[i].servicesCost.reduce(function(prev, cur) {
                return prev + cur;
            });
        };

        $scope.searchKeywords = '';

        $scope.filteredStores = [];

        $scope.row = '';

        $scope.remove = function(sellReport){
          var dlg = $dialogs.confirm('Por favor confirme','Deseja realmente remover?');
          dlg.result.then(function(btn){
            EmporioRestangular.one('posts' , sellReport._id).remove({}, {authorization: localStorage.getItem('currentToken')}).then( function(sellReport) {
              $rootScope.$broadcast('feedbackMessage', {text: 'Post removido com sucesso!', status: 'success'});
              $scope.init();
            }, function(sellReport, err){
              $rootScope.$broadcast('feedbackMessage', {text: 'Ocorreu um erro ao remover o post!', status: 'error'});
              if(sellReport.status === 401) {
                console.log('Não Autorizado')
                localStorage.removeItem('currentToken');
                $state.go('anon.login')
              }
            }); 
          });
        };

        $scope.select = function(page) {
            var end, start;
            start = (page - 1) * $scope.numPerPage;
            end = start + $scope.numPerPage;
            return $scope.itens = $scope.filteredStores.slice(start, end);
        };

        $scope.onFilterChange = function() {
            $scope.select(1);
            $scope.currentPage = 1;
            return $scope.row = '';
        };

        $scope.onNumPerPageChange = function() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.onOrderChange = function() {
            $scope.select(1);
            return $scope.currentPage = 1;
        };

        $scope.search = function() {
            $scope.filteredStores = $filter('filter')($scope.sellReport, $scope.searchKeywords);
            return $scope.onFilterChange();
        };

        $scope.order = function(rowName) {
            if ($scope.row === rowName) {
                return;
            }
            $scope.row = rowName;
            $scope.filteredStores = $filter('orderBy')($scope.sellReport, rowName);
            return $scope.onOrderChange();
        };

        $scope.numPerPageOpt = [3, 5, 10, 20];

        $scope.numPerPage = $scope.numPerPageOpt[2];

        $scope.currentPage = 1;

        $scope.itens = [];

        init = function() {
            $scope.search();
            return $scope.select($scope.currentPage);
        };

        init();
      }, function(sellReport, err){
        if(sellReport.status === 401) {
          console.log('Não Autorizado')
          localStorage.removeItem('currentToken');
          $state.go('anon.login')
        }
      });
    }

    $scope.init();

    //DATEPICKER
    $scope.open = function($event,opened) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope[opened] = true;
    };  
    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
    $scope.format = $scope.formats[2];
    //DATEPICKER

    $scope.$watch('startDate', function(oldValue, newValue) {
      if(oldValue && $scope.verifyDate() && $scope.startDate && $scope.endDate) {
        $scope.reloadSellReports().then(function(sellReport){
          $scope.itens = sellReport;
        });
      }
    });

    $scope.$watch('endDate', function(oldValue, newValue) {
      if(oldValue && $scope.verifyDate() && $scope.startDate && $scope.endDate) {
        $scope.reloadSellReports().then(function(sellReport){
          $scope.itens = sellReport;
        });
      }
    });

    $scope.verifyDate = function(){
      if(moment($scope.startDate).diff(moment($scope.endDate)) <= 0){
        return true
      }else{
        $rootScope.$broadcast('feedbackMessage', {text: 'A data de fim não pode ser menor que a de início!', status: 'error'});
      }
    };

  $scope.reloadSellReports = function() {
    var deferred = $q.defer();
    EmporioRestangular.all('loggedPublisher').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(publisher) {
      $scope.publisher = publisher;
      EmporioRestangular.all('business').getList({}, {authorization: localStorage.getItem('currentToken')}).then( function(sellReport) {
        $scope.sellReport = sellReport;
          $scope.sellReport = _.reduce($scope.sellReport, function(filtered, element){
            if (element.sellDate) {
              filtered.push(element)
            }; 
            return filtered;
          }, []);

          var filteredSellReports = _.reduce($scope.sellReport, function(filtered, element){
            if (moment(element.sellDate, 'YYYY-MM-DD').diff($scope.startDate, 'seconds', true) >= 0 && moment(element.sellDate, 'YYYY-MM-DD').diff($scope.endDate, 'seconds', true) <= 0) {
              filtered.push(element)
            }; 
            return filtered;
          }, []);
          console.log('filteredPosts', filteredSellReports)
          filteredSellReports  = _.sortBy(filteredSellReports, function(element){
            return element.sellDate;
          });
          filteredSellReports = filteredSellReports.reverse();
          deferred.resolve(filteredSellReports);
      }, function(sellReport, err){
        if(sellReport.status === 401) {
          console.log('Não Autorizado')
          localStorage.removeItem('currentToken');
          $state.go('anon.login')
        }
      });
    }, function(publisher, err){
      if(publisher.status === 401) {
        console.log('Não Autorizado')
        localStorage.removeItem('currentToken');
        $state.go('anon.login')
      }
    });

    return deferred.promise;
  };
  }